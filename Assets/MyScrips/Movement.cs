﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/**
 * Movement.cs
 * Luis Diego Fernandez 
*/

public class Movement : NetworkBehaviour
{
    
    public GameObject player;
    public Camera cam;
    public Animator ani;
    public float speed;
    public float rotSpeed;


    // Use this for initialization
    void Start()
    {
        //Posicion original
        player.transform.Translate(0f, 0f, -360f);
        player.transform.Rotate(0f, 180f, 0f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //Codigo de movimiento solo se aplica para los jugadores locales
        if (isLocalPlayer)
        {
            //Camara del jugador
            cam.enabled = true;

            //WASD
            if (Input.GetKey("w"))
            {
                player.transform.Translate(0f, 0f, 1f * speed);
                ani.SetBool("walkForward", true);
            }

            else if (Input.GetKey("a"))
            {
                player.transform.Translate(-1f * speed, 0f, 0f);
                ani.SetBool("walkLeft", true);
            }

            else if (Input.GetKey("d"))
            {
                player.transform.Translate(1f * speed, 0f, 0f);
                ani.SetBool("walkRight", true);
            }

            else
            {
                ani.SetBool("walkForward", false);
                ani.SetBool("walkRight", false);
                ani.SetBool("walkLeft", false);
            }

            //Arrows

            if (Input.GetKey(KeyCode.RightArrow))
            {
                player.transform.Rotate(0f, 1f * rotSpeed, 0f, 0);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                player.transform.Rotate(0f, -1f * rotSpeed, 0f, 0);
            }

        }
    }
}
