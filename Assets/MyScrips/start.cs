﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Movement.cs
 * Luis Diego Fernandez 
*/

public class start : MonoBehaviour {


    public Camera cam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (Input.GetKey(KeyCode.Space))
        {
            //Carga el juego
            SceneManager.LoadScene("main");

        }


	}

    private void FixedUpdate()
    {

        if (cam.transform.position.z < -370)
        {
            cam.transform.Translate(0f,0f,0.3f);
        }


    }

}
